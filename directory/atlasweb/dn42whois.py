import socket

class DN42Whois(object):
    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect(('whois.dn42', 43))

    def whois(self, query):
        self.sock.send(query + '\r\n')
        result = r''
        while True:
            buff = self.sock.recv(4096)
            if not buff:
                break
            result += buff
        return result
