from django.contrib import admin
from atlasweb.models import Probe, PingResult

# Register your models here.
class ProbePingResultInline(admin.TabularInline):
    model = PingResult
    fk_name = 'source_probe'
    readonly_fields = ( 'destination_probe', 'timestamp', 'protocol', 'min', 'max', 'avg', 'jitter', 'loss' )
    ordering = ( '-timestamp', )
    extra = 0
    has_add_permission = lambda x,y : False
    max_num = 20


class ProbeAdmin(admin.ModelAdmin):
    list_display = ( 'name', 'owner', 'id', 'public_id', 'enabled', 'version', 'alive' )
    list_editable = ('enabled', )
    list_filter = ( 'enabled', 'owner', 'version' )
    readonly_fields = ( 'id', 'public_id', 'version', 'last_update')
    actions = ['enable_probe', 'disable_probe']

    def enable_probe(modeladmin, request, queryset):
        queryset.update(enabled=True)
    enable_probe.short_description = "Enable selected probes"

    def disable_probe(modeladmin, request, queryset):
        queryset.update(enabled=False)
    disable_probe.short_description = "Disable selected probes"



admin.site.register(Probe, ProbeAdmin)
