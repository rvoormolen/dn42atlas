from django.conf.urls import url

from . import views
from . import latency_views

# atlasweb.models.Probe.objects.get(id='3abfcf56-5e18-4209-8b6e-db2b9d89ee6e')

UUID_PATTERN  = r'[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}'
UUID_PROBE    = r'(?P<probe>' + UUID_PATTERN + ')'
UUID_SRC_DEST = r'(?P<src_probe>' + UUID_PATTERN + ')/(?P<dst_probe>' + UUID_PATTERN + ')'
PERIODS       = r'(?P<period>(2h|1d|1w|1m|1q|1y))'

urlpatterns = [
    ### Probe API Functions
    ##########################
    url(r'^api/(?P<probe>' + UUID_PATTERN + ')/getprobes/$', views.getprobe),  # Get list of probes
    url(r'^api/(?P<probe>' + UUID_PATTERN + ')/results/$', views.results),    # Post measure results
    url(r'^api/geturl/', views.get_url),                                      # Get URL for probe

    ### GUI Features - MAP
    ##########################
    url(r'^data/probes.js$', views.data_probes),                                              # List of probes (MAP)
    url(r'^data/probe_latency/(?P<probe>' + UUID_PATTERN + ')/$', views.data_probe_latency),  # Probe latency  (MAP)
    url(r'^data/probe_latency/(?P<probe>' + UUID_PATTERN + ')/(?P<direction>(src|dst))/$', views.data_probe_latency), # Probe latency direction (MAP)
    url(r'^data/probe_info/(?P<probe>' + UUID_PATTERN + ')/$', views.data_probe_info),        # Probe information (MAP Modal)

    ### GUI Features - Latency information
    ##########################
    url(r'^data/latency/' + UUID_SRC_DEST + '/latency/$', latency_views.latency),                      # Return graph data of latency between A and B
    url(r'^data/latency/' + UUID_SRC_DEST + '/latency/' + PERIODS + '/$', latency_views.latency),      # Return graph data of latency between A and B (Period)
    url(r'^data/latency/' + UUID_SRC_DEST + '/trace/$', latency_views.trace),                          # Return traceroutes between A and B
    url(r'^data/latency/' + UUID_PROBE    + '/probe/$', latency_views.probe_latency),                  # Return average latency of source probe (avg(avg),avg(max),avg(min))
    url(r'^data/latency/' + UUID_PROBE    + '/probe/' + PERIODS + '/$', latency_views.probe_latency),  # Return average latency of source probe (avg(avg),avg(max),avg(min))
    url(r'^data/latency/' + UUID_SRC_DEST + '/info/$', latency_views.info),                            # Return information about latency measurements (points, latest)

    ### Registration and home
    ##########################
    url(r'^register/$', views.register, name='register'),
    url(r'^data/whois/(?P<query>.*)/$', views.whois),
    url(r'^$', views.home)
]
