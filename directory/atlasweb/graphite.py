import socket
from django.conf import settings

import sys

class Graphite(object):
    def __init__(self, host=settings.GRAPHITE_HOST, port=settings.GRAPHITE_PORT):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.host = host
        self.port = port

    def send(self, data):
        if settings.DEBUG == True:
            sys.stdout.write("[GRAPHITE] %s" % data)
        self.sock.sendto(data, (self.host, self.port))


_graphite = None
def get_graphite():
    global _graphite
    if not _graphite:
        _graphite = Graphite()
    return _graphite
