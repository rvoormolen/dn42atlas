import json
import struct
import pickle
import requests

from django.shortcuts import render, render_to_response
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseNotFound
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db import IntegrityError
from django.views.decorators.http import require_http_methods, require_POST
from django.views.decorators.csrf import csrf_exempt
from django.template import RequestContext
from django.conf import settings
from django.utils import timezone
from django.core.mail import send_mail

from atlasweb import models
from atlasweb import graphite
from atlasweb import dn42whois

from atlasweb.models import probe_uuid_table 
#robe_uuid_table = models.ProbeUUIDTable()

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def get_url(request):
    client = get_client_ip(request)


    p = models.Probe.objects.get(ipv4_address=client)
    
    url = 'http://172.22.189.165:8000/api/%s/getprobes\n' % p.id
    return HttpResponse(url, content_type='text/plain')

def _is_probe_allowed(request, probe):
    return True
    client = get_client_ip(request)

    try:
        requesting_probe = models.Probe.objects.get(id = probe, enabled=True)

        if client != requesting_probe.ipv4_address and client != requesting_probe.ipv6_address:
            return False

    except ObjectDoesNotExist:
        return False

    return True

# Create your views here.
def getprobe(request, probe):

    if not _is_probe_allowed(request, probe):
        return HttpResponseForbidden('No access')

    this_probe = models.Probe.objects.get(id = probe)

    if not this_probe.enabled:
        return HttpResponseForbidden('Probe not enabled')

    probes = []
    for probe in  models.Probe.objects.exclude(id = probe).filter(enabled = True):
        probes.append({
            'name': probe.name,
            'ipv4_address': probe.ipv4_address,
            'ipv6_address': probe.ipv6_address,
        })

    return HttpResponse(json.dumps(probes), content_type='application/json')

@csrf_exempt
@require_POST
def results(request, probe):
    if not _is_probe_allowed(request, probe):
        return HttpResponseForbidden('No access')

    try:
        data = json.loads(request.body)
    except:
        return HttpResponseForbidden('No access')

    source_probe = models.Probe.objects.get(id=probe)
    metrics = []
    timestamp = timezone.now().strftime('%s')
    graphite_conn = graphite.get_graphite()

    # Update Probe metadata
    if 'version' in data:
        source_probe.version = data['version']
    source_probe.last_update = timezone.now()

    for result in data['data']:
        try:
            if data['protocol'] == 4:
                destination_probe = models.Probe.objects.get(ipv4_address=result['address'])
            elif data['protocol'] == 6:
                destination_probe = models.Probe.objects.get(ipv6_address=result['address'])
            else:
                destination_probe = None
        except ObjectDoesNotExist:
            print("Whoops for %s received from %s" % (result['address'], source_probe.name))
            continue

        if destination_probe:
            if data['protocol'] == 4:
                path = '%s.pingresult.%s.%s' % (settings.GRAPHITE_ROOT, source_probe.name.replace('.','_'), destination_probe.name.replace('.','_'))
            if data['protocol'] == 6:
                path = '%s.ping6result.%s.%s' % (settings.GRAPHITE_ROOT, source_probe.name.replace('.','_'), destination_probe.name.replace('.','_'))
            graphite_conn.send("%s %s %d\n" % (path + '.avg',result['avg'], int(timestamp)))
            graphite_conn.send("%s %s %d\n" % (path + '.min',result['min'], int(timestamp)))
            graphite_conn.send("%s %s %d\n" % (path + '.max',result['max'], int(timestamp)))
            graphite_conn.send("%s %s %d\n" % (path + '.loss',result['loss'], int(timestamp)))

    # Save updated probe data
    source_probe.save()

    return HttpResponse('STORED')

def home(request):
    request_context = RequestContext(request)
    request_context.push({"probes": models.Probe.objects.filter(enabled=True)})
    response =  render_to_response('paper.html', request_context)

    response['X-Atlas'] = 'DEV'
    return response

def data_probes(request):
    probes = []
    for probe in models.Probe.objects.filter(enabled = True):
        probes.append({
            'name': probe.name,
            'id': str(probe.public_id),
            'lat': probe.location_lat,
            'lon': probe.location_lon,
        })
   
    return HttpResponse(json.dumps(probes), content_type='application/json')

def data_probe_latency(request, probe, direction='src'):
    try:
       probe = models.Probe.objects.get(public_id=probe)
    except:
        return HttpResponseNotFound('Unfortunately the graph you requested could not be found!')

    try:
        _period = GRAPH_PERIODS[period]
    except:
        _period = '-120min'

    """
    [{"dest": "6a9161d5-c1a1-4720-9a65-ec8606217171", "timestamp": null, "avg": -100, "loss": -100}, {"dest": "c6e06a23-1bfa-421c-b4c2-d5b97b8bdfeb", "timestamp": "2015-04-28 09:42:06+00:00", "avg": 16.5, "loss": 0}, {"dest": "f1df66ef-5d07-4689-ab92-28996641bb4b", "timestamp": null, "avg": -100, "loss": -100}, {"dest": "05fab6d5-438d-4382-968c-e0a01726bcfd", "timestamp": null, "avg": -100, "loss": -100}]
    """
    data = { 'probe': probe.name, 'latency': [] }
    _graph_url = '%s?format=json&from=-15min' % (settings.GRAPHITE_RENDER_URL)
    _probe_name = probe.name.replace('.','_')

    if direction == 'src':
        _graph_url += '&target=%s.pingresult.%s.*.avg' % (settings.GRAPHITE_ROOT, _probe_name)
        _graph_url += '&target=%s.pingresult.%s.*.loss' % (settings.GRAPHITE_ROOT, _probe_name)

    elif direction == 'dst':
        _graph_url += '&target=%s.pingresult.*.%s.avg' % (settings.GRAPHITE_ROOT, _probe_name)
        _graph_url += '&target=%s.pingresult.*.%s.loss' % (settings.GRAPHITE_ROOT, _probe_name)

    data = {}

    try:
        _graph_data = requests.get(_graph_url, timeout=2)
        _graph_metrics = _graph_data.json()
   
        for ds in _graph_metrics:
            target = ds['target']
            
            value = None
            for val in reversed(ds['datapoints']):
                if val[0] != None:
                    value = val[0]
                    break

            dptype = target.split('.')[-1]
            server = target.split('.')[-2]
            if server == _probe_name:
                server = target.split('.')[-3]
            server = server.replace('_','.')

            server = probe_uuid_table.get_by_name(server)

            if server:
                try:
                    data[server][dptype] = value
                except KeyError:
                    data[server] = { dptype: value }

    except MyException as e:
        return HttpResponseNotFound('Unfortunately the graph you requested could not be found!')

    return_data = [ ]
    for key in data:
        dest = key
        try:
            avg = data[key]['avg']
        except KeyError:
            avg = None
        try:
            loss = data[key]['loss']
        except KeyError:
            loss = 100
        return_data.append( {'dest': dest, 'avg': avg, 'loss': loss } )

#    data = [ {'dest': k, 'avg': data[k]['avg'], 'loss': data[k]['loss'] } for k in data ]
    
    #return HttpResponse(json.dumps(_graph_metrics, indent=3), content_type='application/json')
    return HttpResponse(json.dumps(return_data, indent=3), content_type='application/json')
    #requesting_probe = models.Probe.objects.get(public_id = probe, enabled=True)
    #return HttpResponse(json.dumps(requesting_probe.latest_probe_results(True, direction)), content_type='application/json')

def data_probe_info(request, probe):
    requesting_probe = models.Probe.objects.get(public_id = probe, enabled=True)

    data = {
        'public_id': str(requesting_probe.public_id),
        'name': requesting_probe.name,
        'owner': requesting_probe.owner,
        'ipv4_address': requesting_probe.ipv4_address,
        'ipv6_address': requesting_probe.ipv6_address,
        'location': [ requesting_probe.location_lat, requesting_probe.location_lon ],
        'lastupdate': str(requesting_probe.last_update),
    }
    return HttpResponse(json.dumps(data), content_type='application/json')

@require_POST
def register(request):
    new_probe = models.Probe(
        name = request.POST['name'],
        owner = request.POST['owner'],
        location_lat = request.POST['location_lat'],
        location_lon = request.POST['location_lon'],
        ipv4_address = request.POST['ipv4_address']
    )
    data = {}
    try:
        new_probe.clean_fields()
        new_probe.save()
        data = {
            'uuid': str(new_probe.id),
            'registration': 'OK'
        }
    except ValidationError as e:
        data = {
            'registration': 'INVALID',
            'invalid_fields': [ f[0] for f in e ]
        }

    except IntegrityError as e:
        data = {
            'registration': 'FAIL',
        }
        if e[0] == 1062:
            data['error'] = "There is already a node with this name and/or IP address"
        else:
            data['error'] = "Unknown error %d with message '%s'" % (e.__cause__[0], e.__cause__[1])

    subject = '[ATLAS] New probe: %s (%s)' % (new_probe.name, new_probe.owner)
    send_mail(subject, new_probe.view(), 'atlas@atlas.blokje.dn42', ['blokje-dn@42.gs'], fail_silently=False)

    return HttpResponse(json.dumps(data), content_type='application/json')


from django.http import HttpResponseRedirect
def graph_latency(request, src_probe, dst_probe, period):
    try:
        src_probe = models.Probe.objects.get(public_id=src_probe)
        dst_probe = models.Probe.objects.get(public_id=dst_probe)
    except:
        return HttpResponseNotFound('Unfortunately the graph you requested could not be found!')

    path1 = "%s.pingresult.%s.%s.avg" % (settings.GRAPHITE_ROOT, src_probe.name.replace('.','_'), dst_probe.name.replace('.','_'))
    path2 = "%s.pingresult.%s.%s.avg" % (settings.GRAPHITE_ROOT, dst_probe.name.replace('.','_'), src_probe.name.replace('.','_'))
    url = 'http://172.22.189.228:8000/render?target=%s&target=%s&format=json' % (path1, path2)
    return HttpResponseRedirect(url)

# http://morrisjs.github.io/morris.js/

def latency_info(request, src_probe, dst_probe):
    """ Return latency information
        
        Will return a JSON containing traceroute and graphing information """
    try:
        src_probe = models.Probe.objects.get(public_id=src_probe)
        dst_probe = models.Probe.objects.get(public_id=dst_probe)
    except:
        return HttpResponseNotFound('Unfortunately the graph you requested could not be found!')

    data = { 'source': src_probe.name, 'destination': dst_probe.name, 
             'traceroutes': { src_probe.name: [], dst_probe.name: [] } ,
             'latency': [] ,
           }

    _graph_url = '%s?format=json&from=-120min' % settings.GRAPHITE_RENDER_URL
    _graph_url += "&target=%s.pingresult.%s.%s.avg" % (settings.GRAPHITE_ROOT, src_probe.name.replace('.','_'), dst_probe.name.replace('.','_'))
    _graph_url += "&target=%s.pingresult.%s.%s.max" % (settings.GRAPHITE_ROOT, src_probe.name.replace('.','_'), dst_probe.name.replace('.','_'))
    _graph_url += "&target=%s.pingresult.%s.%s.min" % (settings.GRAPHITE_ROOT, src_probe.name.replace('.','_'), dst_probe.name.replace('.','_'))
    _graph_url += "&target=scale(%s.pingresult.%s.%s.avg,-1)" % (settings.GRAPHITE_ROOT, dst_probe.name.replace('.','_'), src_probe.name.replace('.','_'))
    _graph_url += "&target=scale(%s.pingresult.%s.%s.max,-1)" % (settings.GRAPHITE_ROOT, dst_probe.name.replace('.','_'), src_probe.name.replace('.','_'))
    _graph_url += "&target=scale(%s.pingresult.%s.%s.min,-1)" % (settings.GRAPHITE_ROOT, dst_probe.name.replace('.','_'), src_probe.name.replace('.','_'))

    data['_graph_url'] = _graph_url

    try:
        _graph_data = requests.get(_graph_url, timeout=2)
        _graph_metrics = _graph_data.json()
        data['_graph_timing'] = str(_graph_data.elapsed)
        
        for target in _graph_metrics:
            _label = target['target'].split('.')
            _label = '%s -> %s (%s)' % (_label[-3].replace("_", "."), _label[-2].replace("_", "."), _label[-1])
            _data_set = {'label': target['target'], 'data': [] }
            _data_set = {'label': _label, 'data': [] }
            for datapoint in target['datapoints']:
                #_data_set['data'].append([datapoint[1]*1000, datapoint[0]])
                _data_set['data'].append([datapoint[1]*1000, datapoint[0]])
            data['latency'].append(_data_set)

    except Exception as e:
        print("Failure relaying data")

    return HttpResponse(json.dumps(data,indent=3), content_type='application/json')

def probe_availability(request, probe):
    """ Return host availability """
    try:
        probe = models.Probe.objects.get(public_id=probe)
    except:
        return HttpResponseNotFound('Unfortunately the graph you requested could not be found!')


    _graph_url = '%s?format=json&from=-120min' % settings.GRAPHITE_RENDER_URL
    _graph_url += "&target=offset(averageSeries(%s.pingresult.*.%s.loss),100)" % (settings.GRAPHITE_ROOT, probe.name.replace('.','_'))

    data = { "label": "Global availability of %s" % probe.name, 'data': [], 'url': _graph_url }

    try:
        _graph_data = requests.get(_graph_url, timeout=2)
        _graph_metrics = _graph_data.json()
        data['_graph_timing'] = str(_graph_data.elapsed)

        for datapoint in _graph_metrics[0]['datapoints']:
            data['data'].append([datapoint[1]*1000, datapoint[0]])

    except Exception as e:
        print("Failure relaying data")

    return HttpResponse(json.dumps(data,indent=3), content_type='application/json')
  
def whois(request, query):
    whois = dn42whois.DN42Whois()
    info = whois.whois(query)
    return HttpResponse(info, content_type="text/plain")

class MyException(Exception): pass
