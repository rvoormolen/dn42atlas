from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver

from atlasweb.models import Probe
from atlasweb.models import probe_uuid_table

@receiver(post_save, sender=Probe)
def handle_post_save_Probe(sender, **kwargs):
    probe = kwargs['instance']
    
    # Remove from uuid_table
    probe_uuid_table.delete(probe.name)

