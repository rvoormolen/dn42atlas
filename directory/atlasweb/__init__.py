from django.apps import AppConfig

class Config(AppConfig):
    """ Atlasweb configuration

    Primarily used for initializing signals """
    name = 'atlasweb'
    verbose_name = 'DN42 Atlas'

    def ready(self):
        import atlasweb.signals


default_app_config = 'atlasweb.Config'
