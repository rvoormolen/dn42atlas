var ATLASWEB = ATLASWEB || {};
ATLASWEB.Map = ATLASWEB.Map || {};
ATLASWEB.Storage = ATLASWEB.Storage || {};

/** AtlasWeb Constants
 ************************************/
ATLASWEB.c = ATLASWEB.c || {};
ATLASWEB.c.HOST = 'dev.atlas.blokje.dn42'
ATLASWEB.c.PROBE_URI = "http://"+ATLASWEB.c.HOST+"/data/probes.js";
ATLASWEB.c.PROBE_INFO_URI = "http://"+ATLASWEB.c.HOST+"/data/probe_info/";
ATLASWEB.c.LATENCY_URI = "http://"+ATLASWEB.c.HOST+"/data/probe_latency/";
ATLASWEB.c.LATENCY_IMG = '/data/probe_latency/{source}/{destination}/{width}/{period}.png';
ATLASWEB.c.LATENCY_INFO = '/data/latency_info/{source}/{destination}/';
ATLASWEB.c.MAX_LATENCY = 180;   // Max latency in milliseconds
ATLASWEB.c.DATA_LATENCY = '/data/latency/{source}/{destination}/latency/{period}/'; // Information about latency between A and B
ATLASWEB.c.DATA_PROBE = '/data/latency/{source}/probe/{period}/';   // Information about the source probe
ATLASWEB.c.PROBE_SELECT_ZOOM = 9;   // Zoomlevel on probe click

/** AtlasWeb Settings
 ************************************/
ATLASWEB.S = ATLASWEB.S || {};
ATLASWEB.S.LATENCY_DIRECTION = 'src';
ATLASWEB.S.LATENCY_GROUP = L.layerGroup();
ATLASWEB.S.CURRENT_PROBE = null
ATLASWEB.S.PINLOCATION = null
ATLASWEB.S.DATA_PERIOD = '2h';              // Graph period - default 2 hours

/** AtlasWeb functions
 ************************************/
ATLASWEB.fn = ATLASWEB.fn || {};

ATLASWEB.fn.plot_map = function() {
    /** Plot the map in the div with identify 'map' **/
    ATLASWEB.Map._map = L.map('map', {
            zoomControl: false
        })
        .setView([38.59, 25.13], 3);

    // Add zoom control
    ATLASWEB.Map._zoom = new L.Control.Zoom({
            position: 'topright'
        })
        .addTo(ATLASWEB.Map._map);
    $(ATLASWEB.Map._zoom.getContainer()).attr('data-intro', 'Zoom in/out').attr('data-position', 'left')

    // add an OpenStreetMap tile layer
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
        })
        .addTo(ATLASWEB.Map._map);

    // Place the probe direction toggle in the bottomright corner
    ATLASWEB.Map._probedirection = L.Control.extend({
        options: {
            position: 'bottomright'
        },
        onAdd: function(map) {
            return L.DomUtil.get('probedirection');
        }
    });
    ATLASWEB.Map._map.addControl(new ATLASWEB.Map._probedirection());
    $('#probedirection').click(function() {
        ATLASWEB.S.LATENCY_DIRECTION = $(this).find('button.active')[0].value;
        if (ATLASWEB.S.CURRENT_PROBE != null)
            ATLASWEB.fn.probe_click(ATLASWEB.S.CURRENT_PROBE, false)
    });

    
};


ATLASWEB.fn.init_navbar = function() {
    // Quick pan/zoom buttons
    $("#btn-world").click(function(){ ATLASWEB.Map._map.setView([38.59,  25.13], 3); });
    $("#btn-eu").click(function(){    ATLASWEB.Map._map.setView([50.176, 13.227],5); });
    $("#btn-us").click(function(){    ATLASWEB.Map._map.setView([39.300, -95,185],5); });
    $("#btn-help").click(function(){  $('body').chardinJs('start') });

    $("#probeselector").change(function() {
        var marker = ATLASWEB.Storage.Probes[this.value];
        ATLASWEB.fn.probe_click(marker);
    });

    $("#pinlocation").click(function() {
        ATLASWEB.fn.pin_location()
        $('#modal-registration').modal('hide')
    });

    $("#useposition").click(function() {
        $('#modal-registration').modal('show')
    });

    $('#modal-registration').on('show.bs.modal', function() {
        if (ATLASWEB.S.PINLOCATION !== null) {
            ATLASWEB.Map._map.removeLayer(ATLASWEB.S.PINLOCATION);
            ATLASWEB.S.PINLOCATION = null
        }
        $("#pinselectionpanel").addClass('hidden')
    });
};

ATLASWEB.fn.load_probes = function() {
    ATLASWEB.Storage.Probes = ATLASWEB.Storage.Probes || {};

    /** Load the probes and plot them on the map **/
    $.getJSON(ATLASWEB.c.PROBE_URI, function(data) {
        $.each(data, function(index, probe) {
            var marker = L.marker([probe.lat, probe.lon], {
                title: probe.name
            });
            
            marker.probe_id = probe.id;
            marker.name = probe.name;
            marker.on('click', function(e) {
                ATLASWEB.fn.probe_click(this);
            });

            ATLASWEB.Storage.Probes[probe.id] = marker;

            marker.addTo(ATLASWEB.Map._map);
        });
    });
};

ATLASWEB.fn.enable_button_toggle = function() {
    /** Enable button toggle functionality **/
    $('.btn-toggle').click(function() {
        $(this).find('.btn').toggleClass('active');

        if ($(this).find('.btn-primary').size()>0) {
            $(this).find('.btn').toggleClass('btn-primary');
        }
        if ($(this).find('.btn-danger').size()>0) {
            $(this).find('.btn').toggleClass('btn-danger');
        }
        if ($(this).find('.btn-success').size()>0) {
            $(this).find('.btn').toggleClass('btn-success');
        }
        if ($(this).find('.btn-info').size()>0) {
            $(this).find('.btn').toggleClass('btn-info');
        }
        $(this).find('.btn').toggleClass('btn-default');
    });
};

ATLASWEB.fn.latency_color = function(latency, loss) {
    /** Return the color representing latency **/
    if (latency === null) { return '#000000'; }  // Unreachable - Black
    if (latency < 0) { return '#0033ff'; } // No data available - Blue

    // Calculate percentage
    var percentage = latency/ATLASWEB.c.MAX_LATENCY;
    if (percentage > 1) { percentage = 1; }

    var red = ATLASWEB.fn.to_hex(255*percentage);
    var green = ATLASWEB.fn.to_hex(255*(1-percentage));
    var blue = ATLASWEB.fn.to_hex(128*(loss/100));
 
    return ['#',red,green,blue].join('');
};

ATLASWEB.fn.to_hex = function(digit) {
    /** Return hex number **/
    var f = Math.round(digit).toString(16);
    while(f.length < 2)
            f = '0' + f;
    return f;
};

ATLASWEB.fn.probe_click = function(probe_marker, update_info) {
    // Zoom in to probe
    ATLASWEB.Map._map.setView(probe_marker._latlng, ATLASWEB.c.PROBE_SELECT_ZOOM);
    
    // Set as current probe
    ATLASWEB.S.CURRENT_PROBE = probe_marker

    // Load probe information
    if(typeof update_info == 'undefined' || update_info != false) {
        $.getJSON(ATLASWEB.c.PROBE_INFO_URI + probe_marker.probe_id + '/', function(data) {
            $("#probeinfo > .panel-body").html($("#probeInfoTemplate").tmpl(data));
        });
    };

    // Remove all lines currently on the map
    ATLASWEB.Map._map.removeLayer(ATLASWEB.S.LATENCY_GROUP);
    
    var LATENCY_URL = ATLASWEB.c.LATENCY_URI + probe_marker.probe_id + '/' + ATLASWEB.S.LATENCY_DIRECTION + '/';
    $.getJSON(LATENCY_URL, function(data) {
        ATLASWEB.S.LATENCY_GROUP = L.layerGroup();
        var start = probe_marker._latlng;
        $.each(data, function(idx, value) {
            var stop = ATLASWEB.Storage.Probes[value.dest]._latlng;
            var color = ATLASWEB.fn.latency_color(value.avg, value.loss);
            var latencyline = L.polyline([start,stop], {color: color});
            // Add probe information to the latencyline
            latencyline._connected_probes = { 'start': probe_marker.probe_id, 'stop': value.dest } 
            // Click handler  - Show latency graph
            latencyline.on('click', function(e) { ATLASWEB.fn.display_latency(this) });
            latencyline.addTo(ATLASWEB.S.LATENCY_GROUP);
        });
        ATLASWEB.S.LATENCY_GROUP.addTo(ATLASWEB.Map._map);
    });
};

/******** LATENCY INFORMATION ********/
ATLASWEB.fn.init_latencymodal = function() {
    // Initialize latency modal functionality
    $('#latency-info div[role=tabpanel] a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        //console.log(e.target); // newly activated tab
        //console.log(e.relatedTarget); // previous active tab
        ATLASWEB.fn.latency_refresh_active()
    })

    // Refresh graph on period change
    $("#latency-graph-period").change(function() {
        ATLASWEB.S.DATA_PERIOD = this.value;
        ATLASWEB.fn.latency_refresh_active()
    });

    $('#latency_from').change(function() { 
        $("#latency-info").attr('data-source', this.value);
        ATLASWEB.fn.latency_refresh_active();
    });
    $('#latency_to').change(function() { 
        $("#latency-info").attr('data-destination', this.value);
        ATLASWEB.fn.latency_refresh_active();
    });

    // Hide latency info when modal is closed
    $("#latency-info").on('hide.bs.modal', function(e) {
        $("#latency-graph-tooltip").hide();
    });

    // Show/hide latency info when going over the graph
    //    Adjust color with the selected line
    $("#latency-graph").bind("plothover", function(event, pos, item) {

        if(item) {
            _date = item.datapoint[0]

            $("#latency-graph-tooltip").html('Latency: ' + item.datapoint[1].toFixed(2) + 'ms')
                .css({top: item.pageY+5, left: item.pageX+5, 'background-color': item.series.color}).fadeIn(200);
        }
        else {
            $("#latency-graph-tooltip").hide();
        }
    });

}

ATLASWEB.fn.display_latency = function(latencyline) {
    var source = latencyline._connected_probes.start
    var destination = latencyline._connected_probes.stop

    // Set drop down items in title
    $('#latency_to').selectpicker('val', destination);
    $('#latency_from').selectpicker('val', source);

    // Show latency-info form
    $("#latency-info").modal('show');

    // Show first tab
    $('#latency-info div[role=tabpanel] a:first').tab('show');

    // Set source and destination
    $("#latency-info").attr('data-source', source).attr('data-destination', destination);

    // Load graphs in active tab
    ATLASWEB.fn.latency_refresh_active();
}

ATLASWEB.fn.latency_graph_latency = function() {
    var source = $("#latency-info").attr('data-source')
    var destination = $("#latency-info").attr('data-destination')
    var url = ATLASWEB.c.DATA_LATENCY.replace('{source}', source).replace('{destination}', destination).replace('{period}', ATLASWEB.S.DATA_PERIOD)
    $.getJSON(url, function(data) { 
        ATLASWEB.fn.plot_graph("#latency-graph", "#latency-graph-legend", data.latency);
        //console.log(data)
    });
}

ATLASWEB.fn.latency_graph_probe = function() {
    var source = $("#latency-info").attr('data-source')
    var url = ATLASWEB.c.DATA_PROBE.replace('{source}', source).replace('{period}', ATLASWEB.S.DATA_PERIOD)
    $.getJSON(url, function(data) {
        ATLASWEB.fn.plot_graph("#probe-availability-graph", "#probe-availability-graph-legend", data.latency);
        console.log(data)
    })
}

ATLASWEB.fn.latency_refresh_active = function() {
    var active_graph = $('#latency-info div[role=tabpanel] li.active').attr('data-graph')
    if (active_graph == 'latency' ) {
        ATLASWEB.fn.latency_graph_latency()
    }
    else if (active_graph == 'probe') {
        ATLASWEB.fn.latency_graph_probe()
    }
}

ATLASWEB.fn.plot_graph = function(graph, legend, data) {
    var options = {
        series: {
            lines: { show: true },
            points: { show: false },
        },
        xaxis: {
            mode: "time",
            timezone: 'browser',
        },
        grid: {
            hoverable: true,
            autoHighlight: true,
        },
        legend: {
            show: true,
            container: $(legend)
        }
    }

    plot = $(graph).plot(data, options).data("plot");
}

ATLASWEB.fn.plot_latency_graph = function(data) {
    var options = {
        series: {
            lines: { show: true },
            points: { show: false },
        },
        xaxis: {
            mode: "time",
            timezone: 'browser',
        },
        grid: {
            hoverable: true,
            autoHighlight: true,
        },
        legend: {
            show: true,
            container: $("#latency-graph-legend")
        }
    }

    plot = $("#latency-graph").plot(data, options).data("plot");
    
}

/******** CUSTOM ICONS ********/
ATLASWEB.fn.init_icons = function() {
    ATLASWEB.Storage.Icons = ATLASWEB.Storage.Icons || {}

    var shadow = '/static/leaflet/images/marker-shadow.png';
    ATLASWEB.Storage.Icons.red_marker = new L.Icon({
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41],
        iconUrl: '/static/img/marker-red.png',
        shadowUrl: '/static/leaflet/images/marker-shadow.png',
    });
}

/******** REGISTRATION ********/
ATLASWEB.fn.init_registrationform = function() {
    /** Make submit buttons do POST via Ajax **/
    $("#btn-register").click(function(e){
            var required_fields = [ 'name', 'owner', 'ipv4_address', 'location_lat', 'location_lon' ]
            var my_form = $(this).closest('form');
            var form_ok = true;

            $("#registrationerror").addClass('hidden')

            $.each(required_fields, function(idx, value) {
                var field = $(my_form).find('input[name='+value+']')
                var formgroup = $(field).closest('div.form-group')

                if ( $.trim(field.val()).length == 0 ) {
                    $(formgroup).addClass('has-error')
                    form_ok = false;
                }
            })
            if (form_ok === false) {
                $("#registrationerror").removeClass('hidden').html('Not all required fields are supplied')
                return
            };

            $.ajax({type: "POST",
                    url: my_form.attr('action'),
                    data: $(my_form).serialize(),
                    success:function(result){
                        $(my_form).find('.has-error').removeClass('has-error');
                        ATLASWEB.fn.show_registration_status(result, my_form)
                    }});
    });
}

ATLASWEB.fn.show_registration_status = function(result, form) {
    if (result.registration == 'OK') {
        $('#modal-registration').modal('hide');
        $("#registrationerror").addClass('hidden');
        $(form).trigger('reset');
        $("#modal-reg-ok").modal('show').find('strong.reguuid').html(result.uuid);
        return;
    };
    if (result.registration == 'INVALID') {
        $.each(result.invalid_fields, function(idx, field) {
            var ffield = $(form).find('input[name='+field+']');
            var formgroup = $(ffield).closest('div.form-group');
            $(formgroup).addClass('has-error');
            $("#registrationerror").removeClass('hidden').html('Not all fields contain valid information, please fix!');
        });
        return;
    };
    if(result.registration == 'FAIL') {
        $("#registrationerror").removeClass('hidden').html('An error came back from the server<br /><em>' + result.error + "</em>");
        return;
    };
}

ATLASWEB.fn.pin_location = function() {
    ATLASWEB.S.PINLOCATION = new L.marker(ATLASWEB.Map._map.getCenter(), {id:'nothing', draggable:'true', icon: ATLASWEB.Storage.Icons.red_marker})
    $("#pinselectionpanel").removeClass('hidden');

    ATLASWEB.fn.pin_location.show_info(ATLASWEB.Map._map.getCenter());

    ATLASWEB.S.PINLOCATION.on('dragend', function(event){
        var marker = event.target;
        var position = marker.getLatLng();
        ATLASWEB.fn.pin_location.show_info(position);
    });
    ATLASWEB.Map._map.addLayer(ATLASWEB.S.PINLOCATION);
}

ATLASWEB.fn.pin_location.show_info = function(latlng) {
    var lat = latlng.lat.toFixed(5)
    var lon = latlng.lng.toFixed(5)
    $("#registrationform").find('input[name=location_lat]').val(lat)
    $("#registrationform").find('input[name=location_lon]').val(lon)
    $("#pinselectionpanel").find('.position').html(lat + ' / ' + lon)
}

ATLASWEB.fn.whois = function(query) {
    $.get('/data/whois/'+query+'/', function(data) {
        $('#whois-info').text(data);
        $('#modal-whois').modal('show');
    });
}
/**
 ************************************/
/** AtlasWeb Initializer
 ************************************/
ATLASWEB.init = function() {
    ATLASWEB.fn.init_icons();
    ATLASWEB.fn.init_registrationform();
    ATLASWEB.fn.init_latencymodal();
    ATLASWEB.fn.plot_map();
    ATLASWEB.fn.load_probes();
    ATLASWEB.fn.enable_button_toggle();
    ATLASWEB.fn.init_navbar();
};

$(document)
    .ready(function() {
        ATLASWEB.init();
    //    $("#latency-info").modal('show')
    //    ATLASWEB.fn.load_latency_info("c6e06a23-1bfa-421c-b4c2-d5b97b8bdfeb", "05fab6d5-438d-4382-968c-e0a01726bcfd")
    });
