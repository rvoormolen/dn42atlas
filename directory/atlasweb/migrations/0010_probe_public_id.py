# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid

def generate_uuid(apps, schema_editor):
    Probe = apps.get_model('atlasweb', 'Probe')
    for probe in Probe.objects.all().iterator():
        probe.public_id = uuid.uuid4()
        probe.save()

class Migration(migrations.Migration):

    dependencies = [
        ('atlasweb', '0009_auto_20150425_1557'),
    ]

    operations = [
        migrations.AddField(
            model_name='probe',
            name='public_id',
            field=models.UUIDField(editable=False, null=True),
            preserve_default=False,
        ),
        migrations.RunPython(
            generate_uuid,
        ),
        migrations.AlterField(
            model_name='probe',
            name='public_id',
            field=models.UUIDField(unique=True, editable=False),
            preserve_default=True,
        )
    ]
