# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('atlasweb', '0003_auto_20150425_1154'),
    ]

    operations = [
        migrations.AlterField(
            model_name='probe',
            name='last_ping',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
