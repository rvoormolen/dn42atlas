# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('atlasweb', '0002_auto_20150425_1151'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pingresult',
            name='protocol',
            field=models.PositiveSmallIntegerField(choices=[(0, b'IPv4'), (1, b'IPv6')]),
        ),
    ]
