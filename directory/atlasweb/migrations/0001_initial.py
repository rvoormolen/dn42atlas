# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PingResult',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('min', models.FloatField()),
                ('max', models.FloatField()),
                ('avg', models.FloatField()),
                ('jitter', models.FloatField()),
                ('loss', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Probe',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('ipv4_address', models.GenericIPAddressField(null=True, protocol=b'IPv4', blank=True)),
                ('ipv6_address', models.GenericIPAddressField(null=True, protocol=b'IPv6', blank=True)),
                ('owner', models.CharField(max_length=127)),
                ('last_ping', models.DateTimeField()),
                ('location_lat', models.FloatField()),
                ('location_lon', models.FloatField()),
            ],
        ),
        migrations.AddField(
            model_name='pingresult',
            name='destination_probe',
            field=models.ForeignKey(related_name='+', to='atlasweb.Probe'),
        ),
        migrations.AddField(
            model_name='pingresult',
            name='source_probe',
            field=models.ForeignKey(related_name='ping_probes', to='atlasweb.Probe'),
        ),
    ]
