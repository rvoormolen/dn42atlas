# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('atlasweb', '0004_auto_20150425_1155'),
    ]

    operations = [
        migrations.AddField(
            model_name='probe',
            name='name',
            field=models.CharField(default=uuid.uuid4, unique=True, max_length=127),
        ),
        migrations.AlterField(
            model_name='probe',
            name='owner',
            field=models.CharField(default=b'DUMMY-DN42', max_length=127),
        ),
    ]
