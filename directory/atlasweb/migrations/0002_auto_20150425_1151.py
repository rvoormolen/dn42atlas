# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('atlasweb', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='pingresult',
            name='protocol',
            field=models.IntegerField(default=1, max_length=2, choices=[(0, b'IPv4'), (1, b'IPv6')]),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='pingresult',
            name='source_probe',
            field=models.ForeignKey(related_name='ping_probe_result', to='atlasweb.Probe'),
        ),
    ]
