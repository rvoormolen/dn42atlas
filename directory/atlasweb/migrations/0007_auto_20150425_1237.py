# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('atlasweb', '0006_auto_20150425_1158'),
    ]

    operations = [
        migrations.AddField(
            model_name='probe',
            name='enabled',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='pingresult',
            name='avg',
            field=models.FloatField(editable=False),
        ),
        migrations.AlterField(
            model_name='pingresult',
            name='destination_probe',
            field=models.ForeignKey(related_name='+', editable=False, to='atlasweb.Probe'),
        ),
        migrations.AlterField(
            model_name='pingresult',
            name='jitter',
            field=models.FloatField(editable=False),
        ),
        migrations.AlterField(
            model_name='pingresult',
            name='loss',
            field=models.IntegerField(editable=False),
        ),
        migrations.AlterField(
            model_name='pingresult',
            name='max',
            field=models.FloatField(editable=False),
        ),
        migrations.AlterField(
            model_name='pingresult',
            name='min',
            field=models.FloatField(editable=False),
        ),
        migrations.AlterField(
            model_name='pingresult',
            name='protocol',
            field=models.PositiveSmallIntegerField(editable=False, choices=[(0, b'IPv4'), (1, b'IPv6')]),
        ),
        migrations.AlterField(
            model_name='pingresult',
            name='source_probe',
            field=models.ForeignKey(related_name='ping_probe_result', editable=False, to='atlasweb.Probe'),
        ),
        migrations.AlterField(
            model_name='probe',
            name='owner',
            field=models.CharField(max_length=127),
        ),
    ]
