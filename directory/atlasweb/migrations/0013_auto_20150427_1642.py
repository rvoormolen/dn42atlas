# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('atlasweb', '0012_auto_20150426_1834'),
    ]

    operations = [
        migrations.AlterField(
            model_name='probe',
            name='ipv4_address',
            field=models.GenericIPAddressField(unique=True, null=True, protocol=b'IPv4', blank=True),
        ),
    ]
