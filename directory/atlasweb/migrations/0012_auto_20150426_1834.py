# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('atlasweb', '0011_auto_20150425_1945'),
    ]

    operations = [
        migrations.RenameField(
            model_name='probe',
            old_name='last_ping',
            new_name='last_update',
        ),
    ]
