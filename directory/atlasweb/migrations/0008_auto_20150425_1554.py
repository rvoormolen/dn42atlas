# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('atlasweb', '0007_auto_20150425_1237'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pingresult',
            name='avg',
            field=models.FloatField(null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='pingresult',
            name='jitter',
            field=models.FloatField(null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='pingresult',
            name='max',
            field=models.FloatField(null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='pingresult',
            name='min',
            field=models.FloatField(null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='pingresult',
            name='protocol',
            field=models.PositiveSmallIntegerField(editable=False, choices=[(4, b'IPv4'), (6, b'IPv6')]),
        ),
    ]
