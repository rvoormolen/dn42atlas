# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('atlasweb', '0008_auto_20150425_1554'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pingresult',
            name='loss',
            field=models.IntegerField(null=True, editable=False, blank=True),
        ),
    ]
