# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('atlasweb', '0013_auto_20150427_1642'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='probe',
            options={'ordering': ['name']},
        ),
        migrations.AddField(
            model_name='probe',
            name='version',
            field=models.SmallIntegerField(null=True, editable=False, blank=True),
        ),
    ]
