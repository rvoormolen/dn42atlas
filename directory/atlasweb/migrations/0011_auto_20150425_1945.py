# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('atlasweb', '0010_probe_public_id'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='pingresult',
            options={'get_latest_by': 'timestamp'},
        ),
        migrations.AlterField(
            model_name='pingresult',
            name='timestamp',
            field=models.DateTimeField(auto_now_add=True, db_index=True),
        ),
        migrations.AlterField(
            model_name='probe',
            name='public_id',
            field=models.UUIDField(default=uuid.uuid4, unique=True, editable=False),
        ),
    ]
