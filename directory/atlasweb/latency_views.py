import json
import requests

from django.http import HttpResponse, HttpResponseForbidden, HttpResponseNotFound

from django.conf import settings
from atlasweb import models

""" Graphite timing abbreviations
Abbreviation   Unit
           s   Seconds
         min   Minutes
           h   Hours
           d   Days
           w   Weeks
         mon   30 Days (month)
           y   365 Days (year)
"""
GRAPH_PERIODS = {
    '2h': '-120min',
    '1d': '-1d',
    '1w': '-7d',
    '1m': '-1mon',
    '1q': '-3mon',
    '1y': '-1y'
}

def latency(request, src_probe, dst_probe, period="2h"):
    """ Return latency between src_probe and dst_probe for a specific period """
    try:
        src_probe = models.Probe.objects.get(public_id=src_probe)
        dst_probe = models.Probe.objects.get(public_id=dst_probe)
    except:
        return HttpResponseNotFound('Unfortunately the graph you requested could not be found!')

    data = { 'source': src_probe.name, 'destination': dst_probe.name, 
             'latency': [] ,
           }

    try:
        _period = GRAPH_PERIODS[period]
    except:
        print("Resetting period")
        _period = '-120min'

    _graph_url = '%s?format=json&from=%s' % (settings.GRAPHITE_RENDER_URL, _period)
    _graph_url += "&target=%s.pingresult.%s.%s.avg" % (settings.GRAPHITE_ROOT, src_probe.name.replace('.','_'), dst_probe.name.replace('.','_'))
    _graph_url += "&target=%s.pingresult.%s.%s.max" % (settings.GRAPHITE_ROOT, src_probe.name.replace('.','_'), dst_probe.name.replace('.','_'))
    _graph_url += "&target=%s.pingresult.%s.%s.min" % (settings.GRAPHITE_ROOT, src_probe.name.replace('.','_'), dst_probe.name.replace('.','_'))
    _graph_url += "&target=scale(%s.pingresult.%s.%s.avg,-1)" % (settings.GRAPHITE_ROOT, dst_probe.name.replace('.','_'), src_probe.name.replace('.','_'))
    _graph_url += "&target=scale(%s.pingresult.%s.%s.max,-1)" % (settings.GRAPHITE_ROOT, dst_probe.name.replace('.','_'), src_probe.name.replace('.','_'))
    _graph_url += "&target=scale(%s.pingresult.%s.%s.min,-1)" % (settings.GRAPHITE_ROOT, dst_probe.name.replace('.','_'), src_probe.name.replace('.','_'))

    data['_graph_url'] = _graph_url

    try:
        _graph_data = requests.get(_graph_url, timeout=2)
        _graph_metrics = _graph_data.json()
        data['_graph_timing'] = str(_graph_data.elapsed)
        
        for target in _graph_metrics:
            _label = target['target'].split('.')
            _label = '%s -> %s (%s)' % (_label[-3].replace("_", "."), _label[-2].replace("_", "."), _label[-1])
            _data_set = {'label': target['target'], 'data': [] }
            _data_set = {'label': _label, 'data': [] }
            for datapoint in target['datapoints']:
                #_data_set['data'].append([datapoint[1]*1000, datapoint[0]])
                _data_set['data'].append([datapoint[1]*1000, datapoint[0]])
            data['latency'].append(_data_set)

    except Exception as e:
        print("Failure relaying data")

    return HttpResponse(json.dumps(data,indent=3), content_type='application/json')

def probe_latency(request, probe, period="2h"):
    """ Return average latency of source probe (avg(avg),avg(max),avg(min)) """
    try:
       probe = models.Probe.objects.get(public_id=probe)
    except:
        return HttpResponseNotFound('Unfortunately the graph you requested could not be found!')

    try:
        _period = GRAPH_PERIODS[period]
    except:
        _period = '-120min'

    data = { 'probe': probe.name, 'latency': [] }
    _graph_url = '%s?format=json&from=%s' % (settings.GRAPHITE_RENDER_URL, _period)
    _graph_url += '&target=alias(maxSeries(%s.pingresult.*.%s.avg),"Maximum latency to %s")' % (settings.GRAPHITE_ROOT, probe.name.replace('.','_'), probe.name)
    _graph_url += '&target=alias(averageSeries(%s.pingresult.*.%s.avg),"Average latency to %s")' % (settings.GRAPHITE_ROOT, probe.name.replace('.','_'), probe.name)
    _graph_url += '&target=alias(minSeries(%s.pingresult.*.%s.avg),"Minimal latency to %s")' % (settings.GRAPHITE_ROOT, probe.name.replace('.','_'), probe.name)
    _graph_url += '&target=alias(scale(offset(averageSeries(%s.pingresult.*.%s.loss),-100),-1), "Reachability of %s")' % (settings.GRAPHITE_ROOT, probe.name.replace('.','_'), probe.name)

    data['_graph_url'] = _graph_url
    try:
        _graph_data = requests.get(_graph_url, timeout=2)
        _graph_metrics = _graph_data.json()
        data['_graph_timing'] = str(_graph_data.elapsed)

        for dataset in _graph_metrics:
            _data_set = {'label': dataset['target'], 'data': [] }
            for datapoint in dataset['datapoints']:
                _data_set['data'].append([datapoint[1]*1000, datapoint[0]])
            data['latency'].append(_data_set)
    except Exception as e:
        return HttpResponseNotFound('Unfortunately the graph you requested could not be found!')

    return HttpResponse(json.dumps(data,indent=3), content_type='application/json')

def trace(request, src_probe, dst_probe):
    """ Return traceroute from src to dst and reverse """

    data = {
        'src': "Traceroute from source to destination",
        'dst': "Traceroute from destination to source",
    }

    return HttpResponse(json.dumps(data), content_type='application/json')

def info(request, src_probe, dst_probe):
    """ Return information between probes """
    # Return information about latency measurements (points, latest)
    data = {
        'info': 'Sorry, there is currently no information available. Any idea what you would like to see here?'
    }
    return HttpResponse(json.dumps(data), content_type='application/json')
