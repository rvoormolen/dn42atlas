from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone

import uuid

# Create your models here.
class Probe(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    public_id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    name = models.CharField(max_length=127, blank=False, unique=True)
    owner = models.CharField(max_length=127, blank=False)
    version = models.SmallIntegerField(blank=True, null=True, editable=False)

    ipv4_address = models.GenericIPAddressField(protocol='IPv4', blank=True, null=True, unique=True)
    ipv6_address = models.GenericIPAddressField(protocol='IPv6', blank=True, null=True)
    last_update = models.DateTimeField(blank=True, null=True)
    location_lat = models.FloatField()
    location_lon = models.FloatField()

    enabled = models.BooleanField(default=False)

    def latest_probe_results(self, public=False, direction='src'):
        results = []
        for other_probe in Probe.objects.exclude(id = self.id).filter(enabled = True):
            try:
                if direction == 'src':
                    last_probe_result = PingResult.objects.filter(destination_probe=other_probe, source_probe=self).latest()
                else:
                    last_probe_result = PingResult.objects.filter(source_probe=other_probe, destination_probe=self).latest()
            except ObjectDoesNotExist:
                if not public:
                    continue
                else:
                    results.append({
                        'dest': str(other_probe.public_id),
                        'timestamp': None,
                        'avg': -100,
                        'loss': -100
                    })
                    continue

            if not public:
                results.append(last_probe_result)
            else:
                results.append({
                    'dest': str(other_probe.public_id),
                    'timestamp': str(last_probe_result.timestamp),
                    'avg': last_probe_result.avg,
                    'loss': last_probe_result.loss
                })
            
        return results

    def view(self):
        myvars = vars(self)
        return "\n".join(["%s: %s" % (var, myvars[var]) for var in myvars if not var.startswith('_') and myvars[var] is not None])

    def __repr__(self):
        """ Return representation of object """
        myvars = vars(self)
        myrepr = ", ".join(["%s=%s" % (var, myvars[var]) for var in myvars if not var.startswith('_') and myvars[var] is not None])
        return '<Probe(%s)>' % myrepr

    def alive(self):
        if not self.last_update:
            return False

        delta = timezone.now() - self.last_update
    
        if delta.seconds > 3600:
            return False

        return True
    alive.boolean = True

    def __str__(self):
        return self.name

    class Meta:
        ordering = [ 'name' ]

class ProbeUUIDTable(object):
    uuid_table = {}
    def __init__(self):
        pass

    def get_by_name(self, name):
        if name not in self.uuid_table:
            print("Lookup %s" % name)
            try:
                self.uuid_table[name] = str(Probe.objects.get(name=name, enabled=True).public_id)
            except ObjectDoesNotExist:
                self.uuid_table[name] = None
                print("Lookup of %s failed" % name)
                return None
        return self.uuid_table[name]

    def flush(self):
        """ Flush UUID Table """
        self.uuid_table = {}

    def delete(self, name):
        """ Remove from UUID Table """
        print("Delete %s" % name)
        try:
            del self.uuid_table[name]
            print("Sucess")
        except KeyError:
            pass

probe_uuid_table = ProbeUUIDTable()

class PingResult(models.Model):
    PROTOCOL_IPV4 = 4
    PROTOCOL_IPV6 = 6
    PROTOCOL = (
        ( PROTOCOL_IPV4, 'IPv4' ),
        ( PROTOCOL_IPV6, 'IPv6' ),
    )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    source_probe = models.ForeignKey(Probe, related_name='ping_probe_result', editable=False)
    destination_probe = models.ForeignKey(Probe, related_name='+', editable=False, db_index=True)
    timestamp = models.DateTimeField(auto_now_add = True, editable=False, db_index=True)
    protocol = models.PositiveSmallIntegerField(choices=PROTOCOL, editable=False)

    min = models.FloatField(editable=False, blank=True, null=True)
    max = models.FloatField(editable=False, blank=True, null=True)
    avg = models.FloatField(editable=False, blank=True, null=True)
    jitter = models.FloatField(editable=False, blank=True, null=True)
    loss = models.IntegerField(editable=False, blank=True, null=True)

    def __repr__(self):
        """ Return representation of object """
        myvars = vars(self)
        myrepr = ", ".join(["%s=%s" % (var, myvars[var]) for var in myvars if not var.startswith('_') and myvars[var] is not None])
        return '<PingResult(%s)>' % myrepr

    class Meta:
        get_latest_by = "timestamp"
