#!/usr/bin/env python

from __future__ import print_function, unicode_literals, absolute_import

import subprocess
import time
import random
import urllib2
import json
import re

import pprint

# Settings
UUID = 'UUID'

## Advanced settings
BASEURL='http://172.22.189.165:8000/api/%s' % UUID


probes = None

def main():
    global probes

    random.seed()

    while True:
        if not probes:
            print("Get probes")
            probes = getprobes()
            last_update = time.time()

        if (time.time() - last_update) > 300:
            print("Updating probes")
            probes = getprobes()
            last_update = time.time()

        ping4_probes()
        time.sleep(270 + random.randrange(0,30))

def getprobes():
    try:
        f = urllib2.urlopen(BASEURL + '/getprobes/')
        return json.loads(f.read())
    except:
        return None

def probe_addresses_v4():
    global probes
    if not probes:
        return []
    return [ probe['ipv4_address'] for probe in probes ]

def ping4_probes():
    print("Excuting ping")
    with open('/tmp/probes', 'wb') as probefile:
        probefile.write('\n'.join(probe_addresses_v4()))

    fping = subprocess.Popen(['fping', '-q', '-c', '5', '-t', '2500', '-f', '/tmp/probes'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    (results, err) = fping.communicate()
    print(results)
    post_ping4_results(results)

def post_ping4_results(results):
    ##
    # 172.22.189.129 : xmt/rcv/%loss = 1/1/0%, min/avg/max = 9.81/9.81/9.81
    # 172.22.189.241 : xmt/rcv/%loss = 1/1/0%, min/avg/max = 16.5/16.5/16.5
    # 172.22.189.254 : xmt/rcv/%loss = 1/0/100%
    myregex = re.compile(r'(?P<address>\S+)\s+: xmt/rcv/%loss = (?P<xmt>\d+)/(?P<rcv>\d+)/(?P<loss>\d+)%(, min/avg/max = (?P<min>\d*\.?\d+)/(?P<avg>\d*\.?\d+)/(?P<max>\d*\.?\d+))?')


    post = { 'datetime': None, 'my_uuid': UUID, 'protocol': 4, 'data': []}
    for r in results.split('\n'):
        match = myregex.match(r)
        if match:
            post['data'].append(match.groupdict())

    post = json.dumps(post)
    try:
        f = urllib2.urlopen(BASEURL + '/results/', post)
        print("Post OK")
    except:
        print("Post failure - Ignore")

if __name__ == '__main__':
	main()
