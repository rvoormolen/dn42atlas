#!/usr/bin/env python


#### PLEASE NOTE THAT THIS IS WORK-IN-PROGRESS!

from __future__ import print_function, unicode_literals, absolute_import

import subprocess
import time
import random
import urllib2
import json
import re
import os

import pprint

# Settings
UUID = 'YOUR UUID'

## Do not change this unless you know what you are doing :-) (or would like to test against the development server)
ATLAS_SERVER = 'atlas.blokje.dn42'

def main():
    os.umask(0077)

    probe = Probe(UUID, ATLAS_SERVER)
    probe.runloop()


class Probe(object):
    __version__ = 0 # Probe version

    def __init__(self, uuid, atlas_host='atlas.blokje.dn42'):
        self.uuid = uuid
        self.base_url = 'http://%s/api/%s' % (atlas_host, uuid)
        self.probes = None
        self.last_probe_update = time.time()

        # Seed the random pool
        random.seed()

    def runloop(self):
        # Run the Atlas Daemon loop
        while True:
            try:
                if not self.probes:
                    self.refresh_probes()

                # Check if probe list should be updates
                if (time.time() - self.last_probe_update) > 3600:
                    self.refresh_probes()

                self.ping4_probes()

                # Sleep between 270 and 300 seconds
                #time.sleep(270 + random.randrange(0,30))
                time.sleep(120 + random.randrange(0,30))

            except Exception as e:
                print("Uncaught exception: " + e)
                pass

    def ping4_probes(self):
        print("Excuting ping")
        fping = subprocess.Popen(['fping', '-q', '-c', '5', '-t', '2500'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.PIPE)
        (results, err) = fping.communicate(input='\n'.join(self.probe_addresses_v4))
        self.post_ping_results(results, protocol=4)

    def refresh_probes(self):
        print("Updating")
        # Refresh probes from server
        try:
            self.probes = json.loads(urllib2.urlopen(self.base_url + '/getprobes/').read())
            self.last_probe_update = time.time()
        except:
            self.probes = None

    @property
    def probe_addresses_v4(self):
        if not self.probes:
            return []
        return [ probe['ipv4_address'] for probe in self.probes ]


    def post_ping_results(self, results, protocol):
        pingregex = re.compile(r'(?P<address>\S+)\s+: xmt/rcv/%loss = (?P<xmt>\d+)/(?P<rcv>\d+)/(?P<loss>\d+)%(, min/avg/max = (?P<min>\d*\.?\d+)/(?P<avg>\d*\.?\d+)/(?P<max>\d*\.?\d+))?')

        post = { 'datetime': None, 'my_uuid': self.uuid, 'protocol': protocol, 'data': [], 'version': int(self.__version__)}
        for r in results.split('\n'):
            match = pingregex.match(r)
            if match:
                post['data'].append(match.groupdict())
        print(post)
        try:
            f = urllib2.urlopen(self.base_url + '/results/', json.dumps(post))
            print("Post OK")
        except:
            print("Post failure - Ignore")

if __name__ == '__main__':
	main()
